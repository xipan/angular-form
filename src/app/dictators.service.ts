import { Injectable } from '@angular/core';
import { Dictators } from './dictators'

@Injectable({
  providedIn: 'root'
})

export class DictatorsService {
  DictatorsList: Dictators[] = [
    {
      firstname: 'Adolf',
      lastname: 'Hitler',
      dob: new Date('1889-04-20'),
      description: ''
    },
    {       
      firstname: 'Benito',
      lastname: 'Mussolini',
      dob: new Date('1883-07-29'),
      description: ''
    },
  ];

  constructor() { }
}
