import { Component } from '@angular/core';
import { Dictators } from './dictators';
import { DictatorsService } from './dictators.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  dictatorsperson: Dictators;
  dictatorslists: Dictators[];
  firstname: string;
  lastname: string;
  dob: Date;
  description: string;

  constructor(private dService: DictatorsService) {
    this.dictatorslists = dService.DictatorsList;
  }

  ngOnInit() {
    console.log('hello app component');
    console.log(this.dictatorslists);
  }


  onLoginSubmit() {
    this.dictatorsperson = new Dictators(this.firstname, this.lastname, this.dob, this.description);
    this.dictatorslists.push(this.dictatorsperson);
    console.log(this.dictatorslists);
  }

  onDel(index: number) {
    this.dictatorslists.splice(index, 1);//remove element from array
  }

}
