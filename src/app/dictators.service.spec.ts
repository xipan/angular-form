import { TestBed } from '@angular/core/testing';

import { DictatorsService } from './dictators.service';

describe('DictatorsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DictatorsService = TestBed.get(DictatorsService);
    expect(service).toBeTruthy();
  });
});
